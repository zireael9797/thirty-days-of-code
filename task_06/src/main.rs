use std::io;
fn main() {
    let mut inputs: Vec<String> = vec![];
    let mut input_num = String::new();
    io::stdin()
        .read_line(&mut input_num)
        .expect("failed to read from stdin");

    let trimmed = input_num.trim();
    let count = trimmed.parse::<usize>().expect("couldn't read count");
    for _ in 0..count {
        let mut input_text = String::new();
        io::stdin()
            .read_line(&mut input_text)
            .expect("failed to read from stdin");

        let trimmed = input_text.trim().to_string();
        inputs.push(trimmed);
    }
    for i in inputs.iter() {
        let chars = i.chars();

        let mut first: String = String::new();
        let mut second: String = String::new();
        for (i, c) in chars.enumerate() {
            if i % 2 == 0 {
                first.push(c);
            } else {
                second.push(c);
            }
        }
        println!("{} {}", first, second);
    }
}
