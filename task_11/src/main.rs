use std::io;

fn main() {
    let mut line_strings: Vec<String> = Vec::new();
    for _ in 0..6 {
        line_strings.push(input_string());
    }

    let mut main: Vec<Vec<i32>> = Vec::new();
    for line_string in line_strings.iter() {
        let mut line: Vec<i32> = Vec::new();
        let char_array = line_string.split(" ").collect::<Vec<&str>>();
        for char in char_array.iter() {
            let digit: i32 = char.parse::<i32>().expect("error parsing");
            line.push(digit);
        }
        main.push(line);
    }
    let mut max: Option<i32> = None;
    for i in 0..(main.len() - 2) {
        let line = main.get(i).unwrap();
        for j in 0..(line.len() - 2) {
            let mut sum = 0;
            let a = line.get(j).unwrap();
            let b = line.get(j + 1).unwrap();
            let c = line.get(j + 2).unwrap();
            let d = main.get(i + 1).unwrap().get(j + 1).unwrap();
            let third_line = main.get(i + 2).unwrap();
            let e = third_line.get(j).unwrap();
            let f = third_line.get(j + 1).unwrap();
            let g = third_line.get(j + 2).unwrap();
            sum += a + b + c + d + e + f + g;
            match max {
                None => max = Some(sum),
                Some(old_max) => {
                    if sum > old_max {
                        max = Some(sum);
                    }
                }
            }
        }
    }
    println!("{}", max.unwrap());
}

fn input_string() -> String {
    let mut input_string = String::new();
    io::stdin()
        .read_line(&mut input_string)
        .expect("failed to read from stdin");
    input_string.trim().to_string()
}
