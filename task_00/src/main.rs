use std::io::{stdin, stdout, Write};

fn main() {
    let mut input = String::new();
    let flush_result = stdout().flush();
    if let Err(_) = flush_result {
        println!("Error flushing");
    }
    stdin().read_line(&mut input).expect("Invalid Input");
    println!("Hello, World. \n{}", input);
}
