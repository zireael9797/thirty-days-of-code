use std::io;

fn main() {
    let mut input_text = String::new();
    io::stdin()
        .read_line(&mut input_text)
        .expect("failed to read from stdin");

    let trimmed = input_text.trim();
    match trimmed.parse::<u32>() {
        Ok(i) => {
            if i % 2 == 1 || (i % 2 == 0 && i >= 6 && i <= 20) {
                println!("Weird");
            } else {
                println!("Not Weird");
            }
        }
        Err(..) => println!("this was not an integer: {}", trimmed),
    };
}
