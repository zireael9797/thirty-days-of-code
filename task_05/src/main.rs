use std::io;
fn main() {
    let mut input_text = String::new();
    io::stdin()
        .read_line(&mut input_text)
        .expect("failed to read from stdin");

    let trimmed = input_text.trim();
    match trimmed.parse::<u32>() {
        Ok(i) => {
            for n in 1..11 {
                println!("{} x {} = {}", i, n, i * n);
            }
        }
        Err(..) => println!("this was not an integer: {}", trimmed),
    };
}
