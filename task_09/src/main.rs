use std::io;

fn main() {
    let input = input_num();
    println!("{}", factorial(input));
}

fn factorial(input: i64) -> i64 {
    if input == 1 {
        1
    } else {
        input * factorial(input - 1)
    }
}

fn input_string() -> String {
    let mut input_string = String::new();
    io::stdin()
        .read_line(&mut input_string)
        .expect("failed to read from stdin");
    input_string.trim().to_string()
}

fn input_num() -> i64 {
    let input_string = input_string();
    input_string.parse::<i64>().expect("couldn't read count")
}
