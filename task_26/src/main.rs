use std::io;

fn main() {
    let actual = get_date();
    let due = get_date();
    if actual.year > due.year {
        println!("10000");
    } else if actual.month > due.month && actual.year == due.year {
        println!("{}", (actual.month - due.month) * 500);
    } else if actual.day > due.day && actual.month == due.month && actual.year == due.year {
        println!("{}", (actual.day - due.day) * 15);
    } else {
        println!("0")
    }
}

fn input_string() -> String {
    let mut input_string = String::new();
    io::stdin()
        .read_line(&mut input_string)
        .expect("failed to read from stdin");
    input_string.trim().to_string()
}

struct Date {
    day: u64,
    month: u64,
    year: u64,
}

fn get_date() -> Date {
    let date_string = input_string();
    let parts: Vec<&str> = date_string.split(" ").collect();
    Date {
        day: parts[0].parse().expect("couldn't parse day"),
        month: parts[1].parse().expect("couldn't parse month"),
        year: parts[2].parse().expect("couldn't parse year"),
    }
}
