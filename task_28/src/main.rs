use regex::Regex;
use std::io;

fn main() {
    let count = input_num();
    let mut names: Vec<String> = vec![];

    for _ in 0..count {
        let email_regex = Regex::new(
            r"(?P<name>[a-z0-9_+]*) (?P<email>([a-z0-9_+]([a-z0-9_+.]*[a-z0-9_+])?)@gmail.com)",
        )
        .unwrap();
        let input = input_string();

        if let Some(capture) = email_regex.captures(&input) {
            names.push(capture.name("name").unwrap().as_str().to_string());
        }
    }
    names.sort_unstable();
    for name in names {
        println!("{}", name);
    }
}

fn input_string() -> String {
    let mut input_string = String::new();
    io::stdin()
        .read_line(&mut input_string)
        .expect("failed to read from stdin");
    input_string.trim().to_string()
}

fn input_num() -> i64 {
    let input_string = input_string();
    input_string.parse::<i64>().expect("couldn't read count")
}
