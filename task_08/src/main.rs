use std::{collections::HashMap, io};

fn main() {
    let count = input_num();
    let mut phone_book: HashMap<String, String> = HashMap::new();
    for _ in 0..count {
        let input = input_string();
        let pair: Vec<&str> = input.split(" ").collect();
        phone_book.insert(pair[0].to_string(), pair[1].to_string());
    }
    let reader = io::stdin();
    loop {
        let mut buffer: String = String::new();

        if let Ok(nickname) = reader.read_line(&mut buffer) {
            let input = buffer.trim().to_string();
            if nickname == 0 {
                break;
            }
            if let Some(number) = phone_book.get(&input) {
                println!("{}={}", input, number);
            } else {
                println!("Not found");
            }
        } else {
            break;
        }
    }
}

fn input_string() -> String {
    let mut input_string = String::new();
    io::stdin()
        .read_line(&mut input_string)
        .expect("failed to read from stdin");
    input_string.trim().to_string()
}

fn input_num() -> i64 {
    let input_string = input_string();
    input_string.parse::<i64>().expect("couldn't read count")
}
