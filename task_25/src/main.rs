use std::io;

fn main() {
    let count = input_numsize();
    let mut nums: Vec<i64> = vec![];
    for _ in 0..count {
        nums.push(input_num());
    }
    for v in nums {
        let mut prime = if v == 1 { false } else { true };
        let limit = ((v as f64).sqrt() as i64) + 1;
        for d in 2..limit {
            if v % d == 0 {
                prime = false;
                break;
            }
        }
        if prime {
            println!("Prime");
        } else {
            println!("Not prime")
        }
    }
}
fn input_string() -> String {
    let mut input_string = String::new();
    io::stdin()
        .read_line(&mut input_string)
        .expect("failed to read from stdin");
    input_string.trim().to_string()
}

fn input_num() -> i64 {
    let input_string = input_string();
    input_string.parse::<i64>().expect("couldn't read count")
}
fn input_numsize() -> usize {
    let input_string = input_string();
    input_string.parse::<usize>().expect("couldn't read count")
}
