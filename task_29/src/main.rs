use std::io;

fn main() {
    let count = input_num();
    for _ in 0..count {
        let input_string = input_string();
        let mut nums = input_string.split(' ');
        let range = nums.next().unwrap().parse::<usize>().unwrap();
        let max = nums.next().unwrap().parse::<usize>().unwrap();
        let mut max_val = 0_usize;
        for i in 1..range {
            for j in (i + 1)..(range + 1) {
                let bitwise_and = i & j;
                if bitwise_and >= max_val && bitwise_and < max {
                    max_val = bitwise_and;
                }
            }
        }
        println!("{}", max_val);
    }
}
fn input_string() -> String {
    let mut input_string = String::new();
    io::stdin()
        .read_line(&mut input_string)
        .expect("failed to read from stdin");
    input_string.trim().to_string()
}

fn input_num() -> i64 {
    let input_string = input_string();
    input_string.parse::<i64>().expect("couldn't read count")
}
