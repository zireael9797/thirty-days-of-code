package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	var _ = strconv.Itoa // Ignore this comment. You can still use the package "strconv".

	var i uint64 = 4
	var d float64 = 4.0
	var s string = "HackerRank "

	ತ_ʖತ := bufio.NewScanner(os.Stdin)
	// Declare second integer, double, and String variables.
	var iTwo uint64
	var dTwo float64
	var sTwo string
	// Read and save an integer, double, and String to your variables.
	_ = ತ_ʖತ.Scan()
	iTwo, err := strconv.ParseUint(ತ_ʖತ.Text(), 10, 64)
	if err != nil {
		return
	}
	_ = ತ_ʖತ.Scan()
	dTwo, err = strconv.ParseFloat(ತ_ʖತ.Text(), 64)
	if err != nil {
		return
	}
	_ = ತ_ʖತ.Scan()
	sTwo = ತ_ʖತ.Text()
	if err != nil {
		return
	}
	// Print the sum of both integer variables on a new line.
	fmt.Println(i + iTwo)
	// Print the sum of the double variables on a new line.
	fmt.Println(d + dTwo)
	// Concatenate and print the String variables on a new line
	fmt.Println(s + sTwo)
	// The 's' variable above should be printed first.

}
