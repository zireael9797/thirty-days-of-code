use std::io;

fn main() {
    let mut max_count: i64 = 0;
    let mut temp_count: i64 = 0;
    let mut input: i64 = input_num();
    let mut bin: String = "".to_string();
    while input != 0 {
        let digit: i64 = input % 2;

        bin = format!("{}{}", digit, bin);
        input = input / 2;

        if digit == 1 {
            temp_count = temp_count + 1;
        } else {
            if temp_count > max_count {
                max_count = temp_count;
            }
            temp_count = 0;
        }
    }
    if temp_count > max_count {
        max_count = temp_count;
    }
    println!("{}", max_count);
}

fn input_string() -> String {
    let mut input_string = String::new();
    io::stdin()
        .read_line(&mut input_string)
        .expect("failed to read from stdin");
    input_string.trim().to_string()
}

fn input_num() -> i64 {
    let input_string = input_string();
    input_string.parse::<i64>().expect("couldn't read count")
}
